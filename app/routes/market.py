from app.core.routes import TemplateRouter
from app.models.config import RouteConfig

from app.models.dto import (
    CustomerCreate,
    CustomerUpdate,

    ProductCreate,
    ProductUpdate,

    OrderCreate,
    OrderUpdate,

    OrderDetailsCreate,
    OrderDetailsUpdate,
)
from app.models.market import (
    Customer,
    Product,
    Order,
    OrderDetails,
)
from app.repository.store import (
    CustomerRepository,
    ProductRepository,
    OrderRepository,
    OrderDetailsRepository,
)

customer_config = RouteConfig(
    create_model=CustomerCreate,
    update_model=CustomerUpdate,
    response_model=Customer,
    repository=CustomerRepository,
)

product_config = RouteConfig(
    create_model=ProductCreate,
    update_model=ProductUpdate,
    response_model=Product,
    repository=ProductRepository,
)

order_config = RouteConfig(
    create_model=OrderCreate,
    update_model=OrderUpdate,
    response_model=Order,
    repository=OrderRepository,
)

order_details_config = RouteConfig(
    create_model=OrderDetailsCreate,
    update_model=OrderDetailsUpdate,
    response_model=OrderDetails,
    repository=OrderDetailsRepository,
)

customer_routes = TemplateRouter(
    config=customer_config,
    prefix="/customer",
    tags=["Customer"]
)

product_routes = TemplateRouter(
    config=product_config,
    prefix="/product",
    tags=["Product"]
)

order_routes = TemplateRouter(
    config=order_config,
    prefix="/order",
    tags=["Order"]
)

order_details_routes = TemplateRouter(
    config=order_details_config,
    prefix="/order/details",
    tags=["OrderDetails"]
)

customer_routes.generate()
product_routes.generate()
order_routes.generate()
order_details_routes.generate()
