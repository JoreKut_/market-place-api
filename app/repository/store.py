from app.core.repository import BaseRepository
from app.models.market import (
    Customer,
    Product,
    Order,
    OrderDetails,
)
from app.tables.market import (
    customer_table,
    product_table,
    order_table,
    order_details_table,
)


class CustomerRepository(BaseRepository):
    table = customer_table
    model = Customer


class ProductRepository(BaseRepository):
    table = product_table
    model = Product


class OrderRepository(BaseRepository):
    table = order_table
    model = Order


class OrderDetailsRepository(BaseRepository):
    table = order_details_table
    model = OrderDetails
