import sqlalchemy
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Numeric, Date, PrimaryKeyConstraint

from app.core.tables import metadata

# Define tables with snake_case column naming convention
product_table = Table(
    'product', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(255)),
    Column('description', String),
    Column('price', Numeric(10, 2)),
    Column('quantity', Integer)
)

customer_table = Table(
    'customer', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(255)),
    Column('email', String(255)),
    Column('address', String),
    Column('phone_number', String(20))
)

order_table = Table(
    'order', metadata,
    Column('id', Integer, primary_key=True),
    Column('customer_id', Integer, ForeignKey('customer.id')),
    Column('order_date', Date, server_default=sqlalchemy.func.now()),
    Column('total_amount', Numeric(10, 2))
)

order_details_table = Table(
    'order_details', metadata,
    Column('order_id', Integer, ForeignKey('order.id')),
    Column('product_id', Integer, ForeignKey('product.id')),
    Column('quantity', Integer),
    Column('subtotal', Numeric(10, 2)),
    PrimaryKeyConstraint('order_id', 'product_id')
)
