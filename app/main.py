from fastapi import FastAPI

from app.routes.market import (
    customer_routes,
    product_routes,
    order_routes,
    order_details_routes,
)

app = FastAPI()

app.include_router(customer_routes.router)
app.include_router(product_routes.router)
app.include_router(order_routes.router)
app.include_router(order_details_routes.router)
