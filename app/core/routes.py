import typing

from fastapi import APIRouter, Depends, Query, Path

from app.models.config import RouteConfig


class TemplateRouter:
    router: APIRouter

    @classmethod
    def path_id(cls):
        return Path(alias='id')

    def __init__(self, config: RouteConfig, **kwargs):
        self.router = APIRouter(**kwargs)
        self.route_config = config

        if config.retrieve:
            self.retrieve_callback = self.__default_retrieve(config.repository)
        if config.get_all:
            self.get_all_callback = self.__default_get_all(config.repository)
        if config.create:
            self.create_callback = self.__default_create(config.create_model, config.repository)
        if config.update:
            self.update_callback = self.__default_update(config.update_model, config.repository)
        if config.delete:
            self.delete_callback = self.__default_delete(config.repository)

    def retrieve_handler(self, fn):
        """
        Your function should get path_id() param

        ## Example


        ```python
        template = TemplateRouter(...)
        @template.retrieve_handler
        async def foo(your_id=path_id()):
            return {"id": your_id}
        ```
        """
        self.retrieve_callback = fn

    def get_all_handler(self, fn):
        self.get_all_callback = fn

    def create_handler(self, fn):
        self.create_callback = fn

    def update_handler(self, fn):
        """
        Your function should get path_id() param

        ## Example


        ```python
        template = TemplateRouter(...)
        @template.update_handler
        async def foo(your_id=path_id()):
            return {"id": your_id}
        ```
        """
        self.update_callback = fn

    def delete_handler(self, fn):
        """
        Your function should get path_id() param

        ## Example


        ```python
        template = TemplateRouter(...)
        @template.delete_handler
        async def foo(your_id=path_id()):
            return {"id": your_id}
        ```
        """
        self.delete_callback = fn

    def generate(self):
        """Initialize routes with callbacks"""
        if self.retrieve_callback:
            self.router.get(
                "/{id}",
                status_code=200,
                response_model=self.route_config.response_model,
            )(self.retrieve_callback)

        if self.get_all_callback:
            self.router.get(
                "/",
                status_code=200,
                response_model=list[self.route_config.response_model],
            )(self.get_all_callback)

        if self.create_callback:
            self.router.post(
                "/",
                status_code=201,
                response_model=self.route_config.response_model,
            )(self.create_callback)

        if self.update_callback:
            self.router.patch(
                "/{id}",
                status_code=201,
                response_model=self.route_config.response_model,
            )(self.update_callback)

        if self.delete_callback:
            self.router.delete(
                "/{id}",
                status_code=201,
                response_model=self.route_config.response_model,
            )(self.delete_callback)

    @classmethod
    def __default_retrieve(cls, repo_type: typing.Type):
        async def retrieve_entity(
                entity_id=cls.path_id(),
                repo=Depends(repo_type),
        ):
            return await repo.retrieve(id_=entity_id)

        return retrieve_entity

    @classmethod
    def __default_get_all(cls, repo_type: typing.Type, default_limit=30):
        async def get_all(
                limit: int = Query(default=default_limit),
                offset: int = Query(default=None),
                order_by: list = Query(default=None),
                repo=Depends(repo_type),
        ):
            return await repo.get(
                limit=limit,
                offset=offset,
                order_by=order_by,
            )

        return get_all

    @classmethod
    def __default_create(cls, create_model, repo_type: typing.Type):
        async def create_entity(
                cmd: create_model,
                repo=Depends(repo_type),
        ):
            return await repo.create(**cmd.model_dump())

        return create_entity

    @classmethod
    def __default_update(cls, update_model, repo_type: typing.Type):
        async def update_entity(
                cmd: update_model,
                entity_id=cls.path_id(),
                repo=Depends(repo_type),
        ):
            return await repo.update(id_=entity_id, **cmd.model_dump(exclude_unset=True))

        return update_entity

    @classmethod
    def __default_delete(cls, repo_type: typing.Type):
        async def delete_entity(
                entity_id: int = cls.path_id(),
                repo=Depends(repo_type),
        ):
            return await repo.delete(id_=entity_id)

        return delete_entity
