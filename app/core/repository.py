from typing import Optional, Sequence, Type

import sqlalchemy
from fastapi import HTTPException
from pydantic import BaseModel
from sqlalchemy import RowMapping
from sqlalchemy.sql.selectable import TypedReturnsRows, and_

from app.repository.connector import get_session


class BaseRepository:
    """
    Base repository implement basic CRUD methods.
    Just inherit it and redefine `table` and `model` in new Repository class.
    """
    table: sqlalchemy.Table
    model: Type[BaseModel]

    def to_model(self, row: RowMapping) -> 'model':
        return self.model(**dict(row))

    def to_model_list(self, rows: Sequence[RowMapping]):
        return [self.to_model(row) for row in rows]

    async def default_exec_one(self, q: TypedReturnsRows) -> 'model':
        """Return single model"""
        async with get_session() as session:
            result = await session.execute(q)
            map_one_row = result.mappings().fetchone()
            await session.commit()

        if not map_one_row:
            raise HTTPException(status_code=404, detail="Empty Result")

        return self.to_model(map_one_row)

    async def default_exec_many(self, q: TypedReturnsRows):
        """Return list of models"""
        async with get_session() as session:
            result = await session.execute(q)
            map_result_rows = result.mappings().fetchall()
            await session.commit()

        return self.to_model_list(map_result_rows)

    async def retrieve(self, id_: object) -> Optional[BaseModel]:
        """Read row by ID"""
        retrieve_query = self.table.select().where(self.table.c.id == id_)
        return await self.default_exec_one(retrieve_query)

    async def get(self, order_by: list = None, limit=None, offset=None, **kwargs) -> list['model']:
        """Read row by params"""

        get_query = self.table.select()

        where_clause = [
            getattr(self.table.c, k) == v
            for k, v in kwargs.items()
            if k in self.table.c
        ]
        if where_clause:
            get_query = get_query.where(and_(*where_clause))
        if order_by:
            for order_el in order_by:
                get_query = get_query.order_by(order_el)
        if limit:
            get_query = get_query.limit(limit)
        if offset:
            get_query = get_query.offset(offset)

        return await self.default_exec_many(get_query)

    async def create(self, **values) -> Optional['model']:
        """Create row"""
        create_query = self.table.insert().values(values).returning(self.table)
        return await self.default_exec_one(create_query)

    async def update(self, id_: object, **values) -> Optional['model']:
        """Update row by id"""
        update_query = (
            self.table.update()
            .values(values)
            .where(self.table.c.id == id_)
            .returning(self.table)
        )
        return await self.default_exec_one(update_query)

    async def delete(self, id_: object) -> Optional['model']:
        """Delete row by id"""
        delete_query = self.table.delete().where(self.table.c.id == id_).returning(self.table)
        return await self.default_exec_one(delete_query)
