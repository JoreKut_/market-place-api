import json
import os
from functools import lru_cache

import dotenv
from pydantic import SecretStr, Field, BaseModel


class Settings(BaseModel):
    POSTGRES_USER: str = Field(default="postgres")
    POSTGRES_PASSWORD: SecretStr = Field(default=SecretStr("postgres"))
    POSTGRES_DB: str = Field(default="postgres")
    POSTGRES_HOST: str = Field(default="localhost")
    POSTGRES_PORT: str = Field(default="5432")

    @property
    def postgres_url(self):
        return f"postgresql+psycopg2://" \
               f"{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD.get_secret_value()}" \
               f"@{self.POSTGRES_HOST}:{self.POSTGRES_PORT}/{self.POSTGRES_DB}"

    @property
    def async_postgres_url(self):
        return f"postgresql+asyncpg://" \
               f"{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD.get_secret_value()}" \
               f"@{self.POSTGRES_HOST}:{self.POSTGRES_PORT}/{self.POSTGRES_DB}"


@lru_cache
def get_settings() -> Settings:
    dotenv.load_dotenv()
    print("------ GET SETTINGS ------")
    settings = Settings(**os.environ)
    print(json.dumps(settings.model_dump(), default=str, indent=4))
    print("------ END SETTINGS ------")
    return settings
