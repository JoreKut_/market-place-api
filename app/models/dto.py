from typing import Optional

from pydantic import BaseModel


class ProductCreate(BaseModel):
    name: str
    description: Optional[str]
    price: float
    quantity: int


class ProductUpdate(BaseModel):
    name: Optional[str]
    description: Optional[str]
    price: Optional[float]
    quantity: Optional[int]


class CustomerCreate(BaseModel):
    name: str
    email: str
    address: Optional[str]
    phone_number: Optional[str]


class CustomerUpdate(BaseModel):
    name: Optional[str]
    email: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]


class OrderCreate(BaseModel):
    customer_id: int
    total_amount: float


class OrderUpdate(BaseModel):
    customer_id: Optional[int]
    total_amount: Optional[float]


class OrderDetailsCreate(BaseModel):
    order_id: int
    product_id: int
    quantity: int
    subtotal: float


class OrderDetailsUpdate(BaseModel):
    product_id: Optional[int]
    quantity: Optional[int]
    subtotal: Optional[float]
