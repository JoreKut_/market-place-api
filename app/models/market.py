import datetime

from pydantic import BaseModel
from typing import Optional


class Product(BaseModel):
    id: Optional[int]
    name: Optional[str]
    description: Optional[str]
    price: Optional[float]
    quantity: Optional[int]


class Customer(BaseModel):
    id: Optional[int]
    name: Optional[str]
    email: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]


class Order(BaseModel):
    id: Optional[int]
    customer_id: Optional[int]
    order_date: Optional[datetime.date]
    total_amount: Optional[float]


class OrderDetails(BaseModel):
    order_id: int
    product_id: int
    quantity: Optional[int]
    subtotal: Optional[float]
